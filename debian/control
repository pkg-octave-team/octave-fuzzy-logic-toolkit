Source: octave-fuzzy-logic-toolkit
Section: math
Priority: optional
Maintainer: Debian Octave Group <team+pkg-octave-team@tracker.debian.org>
Uploaders: Rafael Laboissière <rafael@debian.org>
Build-Depends: debhelper-compat (= 13),
               dh-sequence-octave
Standards-Version: 4.7.2
Homepage: https://gnu-octave.github.io/packages/fuzzy-logic-toolkit/
Vcs-Git: https://salsa.debian.org/pkg-octave-team/octave-fuzzy-logic-toolkit.git
Vcs-Browser: https://salsa.debian.org/pkg-octave-team/octave-fuzzy-logic-toolkit
Testsuite: autopkgtest-pkg-octave
Rules-Requires-Root: no

Package: octave-fuzzy-logic-toolkit
Architecture: all
Depends: ${misc:Depends}, ${octave:Depends}
Description: fuzzy logic toolkit for Octave
 The Octave Fuzzy Logic Toolkit is a toolkit for Octave, a scientific
 computation software, that provides a large MATLAB compatible subset
 of the functionality of the MATLAB Fuzzy Logic Toolbox as well as
 many extensions. The toolkit includes functions that enable the user
 to build, modify, and evaluate Fuzzy Inference Systems (FISs) from
 the command line and from Octave scripts, read/write FISs to/from
 files, and produce graphical output of both the membership functions
 and the FIS outputs.
 .
 This Octave add-on package is part of the Octave-Forge project.
